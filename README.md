# APOE4 homozygozity represents a distinct genetic form of Alzheimer's disease

This repo contains all the code to reproduce findings from [here]()

## Cite

.. pending full citation..


## Dataset
We did not include the full dataset used in our manuscript for privacy issues.
Each cohort should be accessed individually. 

[WRAP](https://wrap.wisc.edu/data-requests-2/) 

[ADNI](https://adni.loni.usc.edu/data-samples/access-data/)

[OASIS](https://www.oasis-brains.org/)

[ALFA+](https://www.barcelonabeta.org/en/alfa-study/about-the-alfa-study)

[NACC](https://naccdata.org/requesting-data/web-query)

[A4](https://ida.loni.usc.edu/collaboration/access/appLicense.jsp)


We will gladly share the final dataset upon request, after certifying access to the entire set of cohorts.

## Scripts
We provide Rscripts to reproduce the paper figures


## Figures
We do share the .pdf high-resolution figures from the supplementary material, so they can be adequeatly visualized.


## Contact
For further info, contact corresponding authors>
  - Juan Fortea   > jfortea@santpau.cat
  - Victor Montal > victor.montal@protonmail.com